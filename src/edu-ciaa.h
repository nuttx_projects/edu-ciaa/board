/****************************************************************************
 * configs/lpc4357-evb/src/lpc4357-evb.h
 *
 *   Copyright (C) 2014 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef _CONFIGS_CIAA_NXP_LPC4337_H
#define _CONFIGS_CIAA_NXP_LPC4337_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/compiler.h>

#include "lpc43_pinconfig.h"
#include "lpc43_gpio.h"
#include "lpc43_gpioint.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* LED definitions **********************************************************/
/*
 * EDU-CIAA has three standard leds:
 *
 *  ---- ------- -------------
 *  LED  SIGNAL  MCU
 *  ---- ------- -------------
 *	D8    LED1   P2_10
 *	D7    LED2   P2_11
 *	D9    LED3   P2_12
 *  ---- ------- -------------
 *
 * A low output illuminates the LED.
 *
 * Definitions to configure LED pins as GPIOs:
 *
 * - Floating
 * - Normal drive
 * - No buffering, glitch filtering, slew=slow
 */

#define PINCONFIG_LED1  PINCONF_GPIO0p14
#define PINCONFIG_LED2	PINCONF_GPIO1p11
#define PINCONFIG_LED3  PINCONF_GPIO1p12

/* Definitions to configure LED GPIO as outputs */

#define GPIO_LED1       (GPIO_MODE_OUTPUT | GPIO_VALUE_ONE | GPIO_PORT0 | GPIO_PIN14)
#define GPIO_LED2       (GPIO_MODE_OUTPUT | GPIO_VALUE_ONE | GPIO_PORT1 | GPIO_PIN11)
#define GPIO_LED3       (GPIO_MODE_OUTPUT | GPIO_VALUE_ONE | GPIO_PORT1 | GPIO_PIN12)

/* Button definitions *******************************************************/

#define PINCONF_BUTTON1           PINCONF_GPIO0p4
#define PINCONF_BUTTON2           PINCONF_GPIO0p8
#define PINCONF_BUTTON3           PINCONF_GPIO0p9
#define PINCONF_BUTTON4           PINCONF_GPIO1p9

#define GPIO_BUTTON1              (GPIO_PORT0 | GPIO_PIN4)
#define GPIO_BUTTON2              (GPIO_PORT0 | GPIO_PIN8)
#define GPIO_BUTTON3              (GPIO_PORT0 | GPIO_PIN9)
#define GPIO_BUTTON4              (GPIO_PORT1 | GPIO_PIN9)

/****************************************************************************
 * Public Types
 ****************************************************************************/

/****************************************************************************
 * Public data
 ****************************************************************************/

#ifndef __ASSEMBLY__

/****************************************************************************
 * Public Functions
 ****************************************************************************/

int lpc43_bringup(void);

#endif /* __ASSEMBLY__ */
#endif /* _CONFIGS_CIAA_NXP_LPC4337_H */
