#!/bin/bash
####################################################################################
# configs/ciaa-nxp-lpc4337/scripts/flash.sh
#
#   Copyright (C) 2014 Gregory Nutt. All rights reserved.
#   Author: Gregory Nutt <gnutt@nuttx.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name NuttX nor the names of its contributors may be
#    used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
####################################################################################
#set -x

USAGE="$0 <nuttx-path>"

FLASH_WRITE_COMMAND="flash write_image erase"
TARGET_NAME=nuttx
TARGET_DOWNLOAD_EXTENSION=bin
TARGET_DOWNLOAD_FLASH_BASE_ADDR=0x1A000000
export BOARD=edu_ciaa_nxp

NUTTX=$1
if [ -z "${NUTTX}" ]; then
	echo "Missing argument"
	echo $USAGE
	exit 1
fi

if [ ! -d "${NUTTX}" ]; then
	echo "Directory ${NUTTX} does not exist"
	echo $USAGE
	exit 1
fi

# The binary to download:

if [ ! -f "${NUTTX}/${TARGET_NAME}.${TARGET_DOWNLOAD_EXTENSION}" ]; then
	echo "${TARGET_NAME}.${TARGET_DOWNLOAD_EXTENSION} not found in ${NUTTX}"
	exit 1
fi

openocd -f edu-ciaa.cfg -c "init" -c "halt 0" -c "${FLASH_WRITE_COMMAND} ${NUTTX}/${TARGET_NAME}.${TARGET_DOWNLOAD_EXTENSION} ${TARGET_DOWNLOAD_FLASH_BASE_ADDR} ${TARGET_DOWNLOAD_EXTENSION}" -c "reset run" -c "shutdown"
